# ApiRestSeti

Rest Api para última pregunta del examen SETI, en la cual se solicitaba un repositorio para Docker con un proyecto de un API que transforme diversos tipos de peticiones.

### Pedido

Recibe un Json y devuelve una petición xml para SOAP.

_Endpoint: api/Pedido/Pedido_

**Entrada:**

```json
{
	"enviarPedido": {
		"numPedido": "75630275",
		"cantidadPedido": "1",
		"codigoEAN": "00110000765191002104587",
		"nombreProducto": "Armario INVAL",
		"numDocumento": "1113987400",
		"direccion": "CR 72B 45 12 APT 301"
	}
}
```
**Salida:**

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:env="http://WSDLs/EnvioPedidos/EnvioPedidosAcme">
  <soapenv:Header />
  <soapenv:Body>
    <env:EnvioPedidoAcme>
      <EnvioPedidoRequest>
        <pedido>75630275</pedido>
        <Cantidad>1</Cantidad>
        <EAN>00110000765191002104587</EAN>
        <Producto>Armario INVAL</Producto>
        <Cedula>1113987400</Cedula>
        <Direccion>CR 72B 45 12 APT 301</Direccion>
      </EnvioPedidoRequest>
    </env:EnvioPedidoAcme>
  </soapenv:Body>
</soapenv:Envelope>

```

### Respuesta

Transforma la respuesta SOAP XML en JSON

_Endpoint: api/Pedido/Respuesta_
ejemplo:

**Entrada:** 

```xml
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:env="http://WSDLs/EnvioPedidos/EnvioPedidosAcme">
   <soapenv:Header/>
   <soapenv:Body>
      <env:EnvioPedidoAcmeResponse>
         <EnvioPedidoResponse>
            <!--Optional:-->
            <Codigo>80375472</Codigo>
            <!--Optional:-->
            <Mensaje>Entregado exitosamente al cliente</Mensaje>
         </EnvioPedidoResponse>
      </env:EnvioPedidoAcmeResponse>
   </soapenv:Body>
</soapenv:Envelope>
```

**Salida:**

```json
{
	"enviarPedidoRespuesta": {
		"codigoEnvio": "80375472",
		"estado": "Entregado exitosamente al cliente"
	}
}
```

## DOCKER

Comandos para correr docker, una vez ubicado en src/:

`docker build . -t restapiseti`

`docker run -p 5000:5000 -it --rm restapiseti -name restapiseti`

y para abrir Swagger ir al enlace [http://localhost:5000/swagger/index.html](http://localhost:5000/swagger/index.html)
