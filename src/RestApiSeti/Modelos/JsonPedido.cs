﻿using System.ComponentModel.DataAnnotations;

namespace RestApiSeti.Modelos
{
    public class JsonPedido
    {
        [Required]
        public EnvioPedidoRequest EnviarPedido { get; set; }

    }
}
