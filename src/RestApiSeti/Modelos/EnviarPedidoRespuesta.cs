﻿namespace RestApiSeti.Modelos
{
    public class EnviarPedidoRespuesta
    {
        public string CodigoEnvio { get; set; }
        public string Estado { get; set; }
    }
}
