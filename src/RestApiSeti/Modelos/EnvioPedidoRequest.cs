﻿namespace RestApiSeti.Modelos
{
    public class EnvioPedidoRequest
    {
        public string? NumPedido { get; set; }
        public string? CantidadPedido { get; set; }
        public string? CodigoEAN { get; set; }
        public string? NombreProducto { get; set; }
        public string? NumDocumento { get; set; }
        public string? Direccion { get; set; }
    }
}
