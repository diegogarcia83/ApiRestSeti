﻿using Microsoft.AspNetCore.Mvc;
using RestApiSeti.Modelos;
using System.Xml.Linq;

namespace RestApiSeti.Controllers
{
    /// <summary>
    /// Controlador para procesar pedidos.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PedidoController : ControllerBase
    {
        private readonly ILogger<PedidoController> _logger;
        public PedidoController(ILogger<PedidoController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Realiza la transformación del objeto Json del pedido en una salida xml para petición SOAP
        /// </summary>
        /// <param name="envioPedidoRequest">objeto JSON</param>
        /// <returns>Petición SOAP xml</returns>
        [HttpPost("Pedido")]
        [Produces("application/xml")]
        public IActionResult Pedido([FromBody] JsonPedido jsonPedido)
        {
            
            try
            {

                // Convertir JSON a objeto EnvioPedidoRequest
                var envioPedidoRequest = jsonPedido.EnviarPedido;
                // Crear XML con formato requerido
                XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                XNamespace env = "http://WSDLs/EnvioPedidos/EnvioPedidosAcme";
                var xml = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement(soapenv + "Envelope",
                        new XAttribute(XNamespace.Xmlns + "soapenv", soapenv),
                        new XAttribute(XNamespace.Xmlns + "env", env),
                        new XElement(soapenv + "Header"),
                        new XElement(soapenv + "Body",
                            new XElement(env + "EnvioPedidoAcme",
                                new XElement("EnvioPedidoRequest",
                                    new XElement("pedido", envioPedidoRequest.NumPedido),
                                    new XElement("Cantidad", envioPedidoRequest.CantidadPedido),
                                    new XElement("EAN", envioPedidoRequest.CodigoEAN),
                                    new XElement("Producto", envioPedidoRequest.NombreProducto),
                                    new XElement("Cedula", envioPedidoRequest.NumDocumento),
                                    new XElement("Direccion", envioPedidoRequest.Direccion)
                                )
                            )
                        )
                    )
                );

                // Devolver XML como respuesta HTTP
                return Content(xml.ToString(), "text/xml");
                _logger.LogInformation("Se realizó la transformación exitósamente.");
                Console.WriteLine("Se realizó la transformación exitósamente.");
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Se produjo un error: {0}", ex.Message));
                throw ex;
            }
            
        }

        /// <summary>
        /// Realiza una transformación de la respuesta SOAP XML en una respuesta JSON
        /// </summary>
        /// <param name="xml">Response SOAP XML</param>
        /// <returns>Response JSON</returns>
        [HttpPost("Respuesta")]
        [Consumes("application/xml")]        
        public IActionResult Respuesta([FromBody] XElement xml)
        {
            try
            {
                XNamespace env = "http://WSDLs/EnvioPedidos/EnvioPedidosAcme";
                XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                XElement envioResponse = xml.Element(soapenv + "Body")
                    .Element(env + "EnvioPedidoAcmeResponse")
                    .Element("EnvioPedidoResponse");

                string codigo = envioResponse.Element("Codigo")?.Value;

                string mensaje = envioResponse.Element("Mensaje")?.Value;

                var enviarPedidoRespuesta = new EnviarPedidoRespuesta
                {
                    CodigoEnvio = codigo,
                    Estado = mensaje
                };

                return Ok(new { enviarPedidoRespuesta });
                _logger.LogInformation("Se realizó la transformación exitósamente.");
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Se produjo un error: {0}", ex.Message));
                throw ex;
            }

        }
    }
}
